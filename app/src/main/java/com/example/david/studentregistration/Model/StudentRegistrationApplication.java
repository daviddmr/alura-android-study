package com.example.david.studentregistration.Model;

import android.app.Application;

import io.supportkit.core.SupportKit;

/**
 * Created by David on 28/08/15.
 */
public class StudentRegistrationApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SupportKit.init(this, "cz3g0hvscsy93lt6ps0ff1lxx");
    }
}
