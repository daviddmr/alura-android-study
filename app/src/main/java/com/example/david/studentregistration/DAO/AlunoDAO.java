package com.example.david.studentregistration.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.david.studentregistration.ListaAlunosActivity;
import com.example.david.studentregistration.Model.Aluno;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 25/08/15.
 */
public class AlunoDAO extends SQLiteOpenHelper {

    private static final String DATABASE = "AlunosDB";
    private static final int VERSAO = 1;
    private static final String table = "Alunos";

    public AlunoDAO(Context context) {
        super(context, DATABASE, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String sql = "CREATE TABLE " + table + " (" +
                     "id INTEGER PRIMARY KEY, " +
                     "nome TEXT UNIQUE NOT NULL, " +
                     "telefone TEXT, " +
                     "endereco TEXT, " +
                     "site TEXT, " +
                     "nota REAL, " +
                     "urlFoto TEXT " +
                     ");";
        
        database.execSQL(sql);
    }

    //banco, versao antiga do banco e versao nova do banco
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + table;
        database.execSQL(sql);
        onCreate(database);
    }

    public void insere(Aluno aluno) {
        ContentValues cv = new ContentValues();
        cv.put("nome", aluno.getNome());
        cv.put("telefone", aluno.getTelefone());
        cv.put("endereco", aluno.getEndereco());
        cv.put("site", aluno.getSite());
        cv.put("nota", aluno.getNota());
        cv.put("urlFoto", aluno.getUrlFoto());

        getWritableDatabase().insert(table, null, cv);

//        Toast.makeText(ListaAlunosActivity.this, "Aluno cadastrado com sucesso", Toast.LENGTH_SHORT).show();

    }

    public List<Aluno> getLista() {
        ArrayList<Aluno> alunosList = new ArrayList<Aluno>();

        String sql = "SELECT * FROM " + table +";";
        Cursor c = getReadableDatabase().rawQuery(sql, null);

        while(c.moveToNext()){
            Aluno aluno = new Aluno();
            aluno.setId(c.getLong(c.getColumnIndex("id")));
            aluno.setNome(c.getString(c.getColumnIndex("nome")));
            aluno.setTelefone(c.getString(c.getColumnIndex("telefone")));
            aluno.setEndereco(c.getString(c.getColumnIndex("endereco")));
            aluno.setSite(c.getString(c.getColumnIndex("site")));
            aluno.setNota(c.getFloat(c.getColumnIndex("nota")));
            aluno.setUrlFoto(c.getString(c.getColumnIndex("urlFoto")));

            alunosList.add(aluno);
        }

        return alunosList;
    }

    public void delete(Aluno aluno) {
        String[] arrayDelete = {aluno.getId().toString()};
        getWritableDatabase().delete(table, "id=?", arrayDelete);
    }

    public void update(Aluno aluno) {
        ContentValues cv = new ContentValues();
        cv.put("nome", aluno.getNome());
        cv.put("telefone", aluno.getTelefone());
        cv.put("endereco", aluno.getEndereco());
        cv.put("site", aluno.getSite());
        cv.put("nota", aluno.getNota());
        cv.put("urlFoto", aluno.getUrlFoto());
        String[] args = {aluno.getId().toString()};
        getWritableDatabase().update(table, cv, "id=?", args);
    }

    public boolean ehAluno(String telefone) {
        String[] args = {telefone};
        Cursor cursor = getReadableDatabase().rawQuery("Select id from Alunos where telefone = ?", args);
        boolean existeUmPrimeiro = cursor.moveToFirst();
        cursor.close();
        return existeUmPrimeiro;
    }
}
