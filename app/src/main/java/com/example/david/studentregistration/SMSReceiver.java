package com.example.david.studentregistration;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.example.david.studentregistration.DAO.AlunoDAO;

import java.util.Objects;

/**
 * Created by David on 30/09/15.
 */
public class SMSReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Object[] mensagens = (Object[]) intent.getExtras().get("pdus");
        if(mensagens != null) {
            byte[] mensagem = (byte[]) mensagens[0];
            SmsMessage sms = SmsMessage.createFromPdu(mensagem);
            String telefone = sms.getOriginatingAddress();
            boolean smsEhDeAluno = new AlunoDAO(context).ehAluno(telefone);
            if(smsEhDeAluno) {
                MediaPlayer musica = MediaPlayer.create(context, R.raw.facebook_ringtone_pop);
                musica.start();
            }
            Toast.makeText(context, "Sms de aluno: "+ telefone, Toast.LENGTH_SHORT).show();
        }
    }
}
