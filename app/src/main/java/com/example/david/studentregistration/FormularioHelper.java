package com.example.david.studentregistration;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.example.david.studentregistration.Model.Aluno;

/**
 * Created by David on 25/08/15.
 */
public class FormularioHelper {
    private EditText nomeField;
    private EditText siteField;
    private EditText enderecoField;
    private EditText telefoneField;
    private RatingBar notaField;
    private ImageView fotoField;
    private Aluno aluno;

    public FormularioHelper(FormularioActivity activity){
        aluno = new Aluno();

        nomeField = (EditText) activity.findViewById(R.id.nome);
        siteField = (EditText) activity.findViewById(R.id.site);
        enderecoField = (EditText) activity.findViewById(R.id.endereco);
        telefoneField = (EditText) activity.findViewById(R.id.telefone);
        notaField = (RatingBar) activity.findViewById(R.id.nota);
        fotoField = (ImageView) activity.findViewById(R.id.foto);
    }

    public Aluno getAluno(){
        String nome = nomeField.getText().toString();
        String site = siteField.getText().toString();
        String endereco = enderecoField.getText().toString();
        String telefone = telefoneField.getText().toString();
        float nota = notaField.getRating();

        aluno.setNome(nome);
        aluno.setSite(site);
        aluno.setEndereco(endereco);
        aluno.setTelefone(telefone);
        aluno.setNota(nota);

        return aluno;
    }

    public void getInfoAluno(Aluno alunoSelecionado) {
        aluno = alunoSelecionado;
        nomeField.setText(alunoSelecionado.getNome());
        siteField.setText(alunoSelecionado.getSite());
        enderecoField.setText(alunoSelecionado.getEndereco());
        telefoneField.setText(alunoSelecionado.getTelefone());
        notaField.setRating(alunoSelecionado.getNota());

        if(alunoSelecionado.getUrlFoto() != null)
            carregaImagem(alunoSelecionado.getUrlFoto());
    }

    public ImageView getFoto(){
        return fotoField;
    }

    public void carregaImagem(String urlFoto) {
        aluno.setUrlFoto(urlFoto);
        Bitmap imagem = BitmapFactory.decodeFile(urlFoto);
        Bitmap imagemReduzida = Bitmap.createScaledBitmap(imagem, 100, 100, true);
        fotoField.setImageBitmap(imagemReduzida);
    }
}
