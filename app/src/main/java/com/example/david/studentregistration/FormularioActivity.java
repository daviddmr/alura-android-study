package com.example.david.studentregistration;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.david.studentregistration.DAO.AlunoDAO;
import com.example.david.studentregistration.Model.Aluno;

import java.io.File;

/**
 * Created by David on 25/08/15.
 */
public class FormularioActivity extends Activity {

    private String urlFoto;
    private FormularioHelper helper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulario);

        helper = new FormularioHelper(this);

        final Aluno alunoSelecionado = (Aluno) getIntent().getSerializableExtra("alunoSelecionado");

        if(alunoSelecionado != null)
            helper.getInfoAluno(alunoSelecionado);

        final Button btSave = (Button) findViewById(R.id.save);
        if(alunoSelecionado != null)
            btSave.setText("Alterar");
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aluno aluno = helper.getAluno();
                AlunoDAO alunoDAO = new AlunoDAO(FormularioActivity.this);

                if (alunoSelecionado != null) {
                    aluno.setId(alunoSelecionado.getId());
                    alunoDAO.update(aluno);
                } else {
                    alunoDAO.insere(aluno);
                }

                alunoDAO.close();

                finish();
            }
        });

        ImageView foto = helper.getFoto();

        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent abrirCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                urlFoto = getExternalFilesDir(null) + "/" +alunoSelecionado.getNome()+".png";
                File arquivo = new File(urlFoto);
                Uri localFoto = Uri.fromFile(arquivo);
                abrirCamera.putExtra(MediaStore.EXTRA_OUTPUT, localFoto);

                startActivityForResult(abrirCamera, 123);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 123){
            if(resultCode == Activity.RESULT_OK){
                helper.carregaImagem(urlFoto);
            }else{
                urlFoto = null;
            }
        }
    }
}
