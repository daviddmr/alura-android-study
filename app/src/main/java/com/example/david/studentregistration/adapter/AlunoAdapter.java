package com.example.david.studentregistration.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.david.studentregistration.ListaAlunosActivity;
import com.example.david.studentregistration.Model.Aluno;
import com.example.david.studentregistration.R;

import java.util.List;

/**
 * Created by David on 29/09/15.
 */
public class AlunoAdapter extends BaseAdapter{


    private final List<Aluno> alunosList;
    private final Activity activity;

    public AlunoAdapter(List<Aluno> alunosList, Activity activity) {
        this.alunosList = alunosList;
        this.activity = activity;
    }

    //Ensinarmos ao Android quanto itens tem na lista
    @Override
    public int getCount() {
        return alunosList.size();
    }

    @Override
    public Object getItem(int position) {
        return alunosList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return alunosList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        Aluno aluno = getItem(position);
        Aluno aluno = alunosList.get(position);
        LayoutInflater inflater = activity.getLayoutInflater();
        View linha = inflater.inflate(R.layout.item_list, null);

        TextView nome = (TextView) linha.findViewById(R.id.nome);
        nome.setText(aluno.getNome());

        ImageView foto = (ImageView) linha.findViewById(R.id.foto);

        if(aluno.getUrlFoto() != null) {
            Bitmap imagemReduzida = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(aluno.getUrlFoto()), 100, 100, true);
            foto.setImageBitmap(imagemReduzida);
        }else{
            foto.setImageResource(R.drawable.ic_photo);
        }

        return linha;
    }
}
