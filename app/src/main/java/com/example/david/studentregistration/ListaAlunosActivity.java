package com.example.david.studentregistration;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.david.studentregistration.DAO.AlunoDAO;
import com.example.david.studentregistration.Model.Aluno;
import com.example.david.studentregistration.adapter.AlunoAdapter;

import java.util.List;


public class ListaAlunosActivity extends Activity {

    String tag = "Hello World";
    ListView lista;
    AlunoAdapter adapter;
    ArrayAdapter<Aluno> adapter2;
    private Aluno aluno;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listagem_alunos);

        lista = (ListView) findViewById(R.id.lista);
        registerForContextMenu(lista); //É necessário registrar para contexto caso deseje usar a lista de menu flutuante na view


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(ListaAlunosActivity.this, "Item " + (position+1) + " clicado", Toast.LENGTH_SHORT).show();
                Aluno alunoSelecionado = (Aluno) adapter.getItem(position);
                Intent irParaoFormulario = new Intent(ListaAlunosActivity.this, FormularioActivity.class);
                irParaoFormulario.putExtra("alunoSelecionado", alunoSelecionado);

                startActivity(irParaoFormulario);
            }
        });

        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                aluno = (Aluno) adapter.getItem(position);
//                Toast.makeText(ListaAlunosActivity.this, "Aluno: "+ position, Toast.LENGTH_SHORT).show();
//                return true; //para caso deseje que o método execute sozinho
                return false; //para caso deseje que o método execute algo depois desse método
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        menu.add(R.menu.menu_main);
        getMenuInflater().inflate(R.menu.menu_lista_alunos, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.novo:
                Intent irParaFormulario = new Intent(this, FormularioActivity.class);
                startActivity(irParaFormulario);
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadList();
    }

    private void loadList() {
        AlunoDAO alunoDAO = new AlunoDAO(this);
        List<Aluno> alunosList = alunoDAO.getLista();

        //Contexto, layout de exibicao, conteudo
        adapter = new AlunoAdapter(alunosList, this);
//        adapter = new ArrayAdapter<Aluno>(this, R.layout.item_list, alunosList);
        lista.setAdapter(adapter);

        getLayoutInflater();
    }

    //Menu flutuante (Botão direito do mouse)
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //Menu programático
        MenuItem ligar = menu.add("Ligar");
        ligar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                //Aproveitando uma itent nativa para a tela de discagem
                Intent irParaTelaDiscagem = new Intent(Intent.ACTION_CALL);
                Uri telefoneDoAluno = Uri.parse("tel:" + aluno.getTelefone());
                irParaTelaDiscagem.setData(telefoneDoAluno);
                startActivity(irParaTelaDiscagem);

                return false;
            }
        });
        menu.add("Enviar SMS");
        menu.add("Achar no Mapa");
        MenuItem navegarNoSite = menu.add("Navegar no site");
        navegarNoSite.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent irParaNavegador = new Intent(Intent.ACTION_VIEW);
                Uri siteDoAluno = Uri.parse("http://" + aluno.getSite());
                irParaNavegador.setData(siteDoAluno);

                startActivity(irParaNavegador);
                return false;
            }
        });
        MenuItem deletar = menu.add("Deletar");
        //Adicionando um ouvinte ao item do menu deletar
        deletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AlunoDAO alunoDAO = new AlunoDAO(ListaAlunosActivity.this);
                alunoDAO.delete(aluno);
                alunoDAO.close();

                loadList();
                return false;
            }
        });
        menu.add("Enviar E-mail");

    }

    //Caso quisesse fazer leitura do menu do "Botão direito"
//    @Override
//    public boolean onContextItemSelected(MenuItem item) {
//        return super.onContextItemSelected(item);
//    }
}
